package ru.mip.database.classes;

import javafx.collections.ObservableList;
import javafx.scene.control.Tab;
import javafx.scene.control.TableView;
import ru.mip.database.tables.TableGroupModel;
import ru.mip.database.tables.TableStudentsModel;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class RefreshTable {
    
    public void refreshTablePeople(ObservableList<TableStudentsModel> listView, TableView<TableStudentsModel> table_people, Tab tab_students, Connection connection) {
        listView.clear();
        try {
            String query = "select * from people";
            Statement statement = connection.createStatement();
            ResultSet set = statement.executeQuery(query);

            while (set.next()){
                listView.add(new TableStudentsModel(
                        set.getString("ID"),
                        set.getString("NAME"),
                        set.getString("CLASS"),
                        set.getString("SCORE"),
                        set.getString("BIRTHDAY"),
                        set.getString("SEX")));
            }
            table_people.setItems(listView);
            tab_students.setContent(table_people);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void refreshTableGroup(ObservableList<TableGroupModel> list, TableView<TableGroupModel> table_group, Tab tab_group, Connection connection) {
        list.clear();
        try {
            String query = "select * from class";
            Statement statement = connection.createStatement();
            ResultSet set = statement.executeQuery(query);

            while (set.next()){
                list.add(new TableGroupModel(
                        set.getString("ID"),
                        set.getString("NAME"),
                        set.getString("FACULTY")));
            }
            table_group.setItems(list);
            tab_group.setContent(table_group);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
