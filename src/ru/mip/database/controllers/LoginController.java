package ru.mip.database.controllers;

import animatefx.animation.BounceIn;
import animatefx.animation.ZoomIn;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ResourceBundle;

public class LoginController implements Initializable {

    public Button button_student;
    public Button button_customer;
    public Button button_exit;
    public AnchorPane anchor_pane;
    public ImageView image_custom;
    public ImageView image_student;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        button_student.setOnMouseEntered(e -> button_student.setCursor(Cursor.OPEN_HAND));
        button_customer.setOnMouseEntered(e -> button_customer.setCursor(Cursor.OPEN_HAND));
        button_exit.setOnMouseEntered(e -> button_exit.setCursor(Cursor.OPEN_HAND));

        Tooltip tooltipCustomer = new Tooltip();
        tooltipCustomer.setText("Просмотр, добавление, удаление\n " +
                "и изменение записей");
        button_customer.setTooltip(tooltipCustomer);

        Tooltip tooltipStudent = new Tooltip();
        tooltipStudent.setText("Просмотр записей");
        button_student.setTooltip(tooltipStudent);
        new BounceIn(button_customer).play();
        new BounceIn(button_student).play();
    }

    public void studentAction(ActionEvent actionEvent) throws IOException {
        URL url = new File("src/ru/mip/database/fxmlfiles/sign_in_student.fxml").toURI().toURL();
        AnchorPane pane = FXMLLoader.load(url);
        anchor_pane.getChildren().setAll(pane);
    }

    public void customerAction(ActionEvent actionEvent) throws IOException {
        URL url = new File("src/ru/mip/database/fxmlfiles/sign_in_customer.fxml").toURI().toURL();
        AnchorPane pane = FXMLLoader.load(url);
        anchor_pane.getChildren().setAll(pane);

    }

    public void exitAction(ActionEvent actionEvent) throws IOException {
        System.exit(0);
    }
}
