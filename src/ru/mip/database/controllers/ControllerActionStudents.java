package ru.mip.database.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import ru.mip.database.connections.ConnectionDB;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

public class ControllerActionStudents implements Initializable {

    public TextField field_id;
    public TextField field_name;
    public TextField field_group;
    public TextField field_score;
    public TextField field_birth;
    public TextField field_sex;
    public Button button_save;
    public Button button_back;
    public AnchorPane anchor_pane_add;
    public Button button_back1;
    public Button button_change;
    public TextField field_id_changed;
    public TextField field_name_changed;
    public TextField field_group_changed;
    public TextField field_score_changed;
    public TextField field_birth_changed;
    public TextField field_sex_changed;
    public Label label_error;
    ConnectionDB connectionDB = new ConnectionDB();
    Connection connection = connectionDB.databaseConnection();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    public void saveAction(ActionEvent actionEvent) {
        try {
            String id = field_id.getText();
            String name = field_name.getText();
            String group = field_group.getText();
            String score = field_score.getText();
            String birthday = field_birth.getText();
            String sex = field_sex.getText();

            String insertValues = "INSERT INTO PEOPLE(ID, NAME, CLASS, SCORE, BIRTHDAY, SEX) VALUES ('";
            String insertFields = id + "','" + name + "','" + group + "','" + score + "','" + birthday + "','" + sex +"')";
            String insertAdd = insertValues + insertFields;

            Statement statement = connection.createStatement();
            statement.executeUpdate(insertAdd);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void backAction(ActionEvent actionEvent) throws IOException {
        URL url = new File("src/ru/mip/database/fxmlfiles/database.fxml").toURI().toURL();
        AnchorPane pane = FXMLLoader.load(url);
        anchor_pane_add.getChildren().setAll(pane);
    }

    public void changeAction(ActionEvent actionEvent) throws SQLException {
        String id = field_id_changed.getText();
        String name = field_name_changed.getText();
        String group = field_group_changed.getText();
        String score = field_score_changed.getText();
        String birthday = field_birth_changed.getText();
        String sex = field_sex_changed.getText();

        label_error.setVisible(id.isEmpty());

        if(!name.isEmpty()){
            String insertValues = "UPDATE PEOPLE SET " +
                    "NAME = '" + name +"'" +
                    " WHERE ID = '" + id + "'";

            Statement statement = connection.createStatement();
            statement.executeUpdate(insertValues);
        }
        if (!group.isEmpty()){
            String insertValues = "UPDATE PEOPLE SET " +
                    "CLASS = '" + group + "'" +
                    " WHERE ID = '" + id + "'";

            Statement statement = connection.createStatement();
            statement.executeUpdate(insertValues);
        }
        if (!score.isEmpty()){
            String insertValues = "UPDATE PEOPLE SET " +
                    "SCORE = '" + score + "'" +
                    " WHERE ID = '" + id + "'";

            Statement statement = connection.createStatement();
            statement.executeUpdate(insertValues);
        }
        if (!birthday.isEmpty()){
            String insertValues = "UPDATE PEOPLE SET " +
                    "BIRTHDAY = '" + birthday + "'" +
                    " WHERE ID = '" + id + "'";

            Statement statement = connection.createStatement();
            statement.executeUpdate(insertValues);
        }
        if (!sex.isEmpty()){
            String insertValues = "UPDATE PEOPLE SET " +
                    "SEX = '" + sex + "'" +
                    " WHERE ID = '" + id + "'";

            Statement statement = connection.createStatement();
            statement.executeUpdate(insertValues);
        }
    }
}
