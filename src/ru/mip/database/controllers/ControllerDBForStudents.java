package ru.mip.database.controllers;

import animatefx.animation.FadeIn;
import animatefx.animation.ZoomIn;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import ru.mip.database.classes.RefreshTable;
import ru.mip.database.connections.ConnectionDB;
import ru.mip.database.tables.TableGroupModel;
import ru.mip.database.tables.TableStudentsModel;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

public class ControllerDBForStudents implements Initializable {

    public TableView<TableStudentsModel> table_people;
    public TableColumn<TableStudentsModel, Integer> stident_id;
    public TableColumn<TableStudentsModel, String> student_name;
    public TableColumn<TableStudentsModel, String> student_group;
    public TableColumn<TableStudentsModel, Integer> student_score;
    public TableColumn<TableStudentsModel, String> student_birth;
    public TableColumn<TableStudentsModel, Integer> student_sex;

    public Button button_close1;
    public Button button_exit;
    public AnchorPane anchor_pane2;

    public TableColumn<TableGroupModel, Integer> customer_id;
    public TableColumn<TableGroupModel, String> customer_name;
    public TableColumn<TableGroupModel, String> customer_faculty;
    public TableView<TableGroupModel> table_group;
    public Tab tab_students;
    public Tab tab_groups;
    public TabPane tab_pane;
    public AnchorPane anchor_pane_action;
    public Button button_refresh;
    public TextField field_search;
    public TextField field_search1;
    ConnectionDB connectionDB = new ConnectionDB();
    Connection connection = connectionDB.databaseConnection();

    ObservableList<TableStudentsModel> listView = FXCollections.observableArrayList();
    ObservableList<TableGroupModel> list = FXCollections.observableArrayList();

    public void exitAction(ActionEvent actionEvent) {
        System.exit(0);
    }

    public void buttonClose1(ActionEvent actionEvent) throws IOException {
        new FadeIn(anchor_pane2).play();
        URL url = new File("src/ru/mip/database/fxmlfiles/login.fxml").toURI().toURL();
        AnchorPane pane = FXMLLoader.load(url);
        anchor_pane2.getChildren().setAll(pane);
    }

    public void refreshAction(ActionEvent actionEvent) {
        RefreshTable refreshTable = new RefreshTable();
        refreshTable.refreshTablePeople(listView, table_people, tab_students, connection);
        refreshTable.refreshTableGroup(list, table_group, tab_groups, connection);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        button_close1.setOnMouseEntered(e -> button_close1.setCursor(Cursor.OPEN_HAND));
        button_exit.setOnMouseEntered(e -> button_exit.setCursor(Cursor.OPEN_HAND));
        button_refresh.setOnMouseEntered(e -> button_refresh.setCursor(Cursor.OPEN_HAND));
        new ZoomIn(anchor_pane2).play();

        Tooltip tooltipDelete = new Tooltip();
        tooltipDelete.setText("Поиск стдента в БД");
        field_search.setTooltip(tooltipDelete);

        Tooltip tooltipDelete1 = new Tooltip();
        tooltipDelete1.setText("Поиск группы в БД");
        field_search1.setTooltip(tooltipDelete1);

        Tooltip tooltipRefresh = new Tooltip();
        tooltipRefresh.setText("Обновить БД");
        button_refresh.setTooltip(tooltipRefresh);

        filtredDataStudents();

        filtredDataGroups();
    }

    private void filtredDataGroups() {
        customer_id.setCellValueFactory(new PropertyValueFactory<>("id"));
        customer_name.setCellValueFactory(new PropertyValueFactory<>("name"));
        customer_faculty.setCellValueFactory(new PropertyValueFactory<>("faculty"));

        try {
            ConnectionDB connectionDB = new ConnectionDB();
            Connection connection = connectionDB.databaseConnection();

            String sqlCustomers = "SELECT * FROM CLASS";
            Statement s1 = connection.createStatement();
            ResultSet r1 = s1.executeQuery(sqlCustomers);

            while (r1.next()){
                list.add(new TableGroupModel(
                        r1.getString("id"),
                        r1.getString("name"),
                        r1.getString("faculty")
                ));
            }
            table_group.setItems(list);
            tab_groups.setContent(table_group);

            FilteredList<TableGroupModel> filteredList = new FilteredList<>(list, e -> true);
            field_search1.textProperty().addListener(((observable, oldValue, newValue) -> {
                filteredList.setPredicate(employee -> {
                    if (newValue == null || newValue.isEmpty()){
                        return true;
                    }
                    String lowerCaseFilter = newValue.toLowerCase();
                    if (employee.getName().toLowerCase().contains(lowerCaseFilter)){
                        return true;
                    } else if (employee.getFaculty().toLowerCase().contains(lowerCaseFilter)){
                        return true;
                    } else if (employee.getId().toLowerCase().contains(lowerCaseFilter)){
                        return true;
                    }
                    else
                        return false;
                });
            }));
            SortedList<TableGroupModel> sortedList = new SortedList<>(filteredList);
            sortedList.comparatorProperty().bind(table_group.comparatorProperty());
            table_group.setItems(sortedList);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void filtredDataStudents() {
        stident_id.setCellValueFactory(new PropertyValueFactory<>("id"));
        student_name.setCellValueFactory(new PropertyValueFactory<>("name"));
        student_group.setCellValueFactory(new PropertyValueFactory<>("group"));
        student_score.setCellValueFactory(new PropertyValueFactory<>("score"));
        student_birth.setCellValueFactory(new PropertyValueFactory<>("birthday"));
        student_sex.setCellValueFactory(new PropertyValueFactory<>("sex"));

        try {
            String sql = "SELECT * FROM PEOPLE";
            Statement s = connection.createStatement();
            ResultSet r = s.executeQuery(sql);

            while (r.next()){
                listView.add(new TableStudentsModel(
                        r.getString("ID"),
                        r.getString("NAME"),
                        r.getString("CLASS"),
                        r.getString("SCORE"),
                        r.getString("BIRTHDAY"),
                        r.getString("SEX")));
            }
            table_people.setItems(listView);
            tab_students.setContent(table_people);

            FilteredList<TableStudentsModel> filteredList = new FilteredList<>(listView, e -> true);
            field_search.textProperty().addListener(((observable, oldValue, newValue) -> {
                filteredList.setPredicate(employee -> {
                    if (newValue == null || newValue.isEmpty()){
                        return true;
                    }
                    String lowerCaseFilter = newValue.toLowerCase();
                    if (employee.getName().toLowerCase().contains(lowerCaseFilter)){
                        return true;
                    } else if (employee.getGroup().toLowerCase().contains(lowerCaseFilter)){
                        return true;
                    } else if (employee.getId().toLowerCase().contains(lowerCaseFilter)){
                        return true;
                    } else if (employee.getBirthday().toLowerCase().contains(lowerCaseFilter)){
                        return true;
                    } else if (employee.getScore().toLowerCase().contains(lowerCaseFilter)){
                        return true;
                    } else return employee.getSex().toLowerCase().contains(lowerCaseFilter);
                });
            }));
            SortedList<TableStudentsModel> sortedList = new SortedList<>(filteredList);
            sortedList.comparatorProperty().bind(table_people.comparatorProperty());
            table_people.setItems(sortedList);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }


}
