package ru.mip.database.controllers;

import animatefx.animation.FadeIn;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import ru.mip.database.connections.ConnectionDB;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

public class RegistartionController implements Initializable {

    public AnchorPane anchor_pane;
    public Button button_exit;
    public TextField field_name;
    public TextField field_lastname;
    public TextField field_login;
    public PasswordField field_password;
    public Button button_registartion;
    public Button button_back;
    public PasswordField field_password_repeat;
    public Label label_error;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        button_back.setOnMouseEntered(e -> button_back.setCursor(Cursor.OPEN_HAND));
        button_exit.setOnMouseEntered(e -> button_exit.setCursor(Cursor.OPEN_HAND));
        button_registartion.setOnMouseEntered(e -> button_registartion.setCursor(Cursor.OPEN_HAND));
    }

    public void exitAction(ActionEvent actionEvent) {
        System.exit(0);
    }

    public void registartionAction(ActionEvent actionEvent) throws SQLException, IOException {
        if (field_password.getText().equals(field_password_repeat.getText())){
            registerUser();
        }else {
            label_error.setText("Пароли не совпадают!");
        }
    }

    private void registerUser() throws SQLException, IOException {
        ConnectionDB connectionDB = new ConnectionDB();
        Connection connection = connectionDB.databaseConnection();

        String firstname = field_name.getText();
        String lastname = field_lastname.getText();
        String username = field_login.getText();
        String password = field_password.getText();

        String insertFields = "insert into login(firstname, lastname, username, password) values ('";
        String insertValues = firstname + "','" + lastname + "','" + username + "','" + password + "')";
        String sql = insertFields + insertValues;

        Statement statement = connection.createStatement();
        statement.executeUpdate(sql);

        URL url = new File("src/ru/mip/database/fxmlfiles/database.fxml").toURI().toURL();
        AnchorPane pane = FXMLLoader.load(url);
        anchor_pane.getChildren().setAll(pane);
    }

    public void backAction(ActionEvent actionEvent) throws IOException {
        new FadeIn(anchor_pane).play();
        URL url = new File("src/ru/mip/database/fxmlfiles/sign_in_customer.fxml").toURI().toURL();
        AnchorPane pane = FXMLLoader.load(url);
        anchor_pane.getChildren().setAll(pane);
    }
}
