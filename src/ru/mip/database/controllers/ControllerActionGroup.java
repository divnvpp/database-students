package ru.mip.database.controllers;

import animatefx.animation.FadeIn;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.AnchorPane;
import ru.mip.database.connections.ConnectionDB;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

public class ControllerActionGroup implements Initializable {
    public TextField field_id1;
    public AnchorPane anchor_pane_add;
    public TextField field_group1;
    public TextField field_faculty;
    public Button button_back1;
    public Button button_save1;
    public TextField field_id1_changed;
    public TextField field_group1_changed;
    public TextField field_faculty_changed;
    public Button button_back2;
    public Button button_update1;
    ConnectionDB connectionDB = new ConnectionDB();
    Connection connection = connectionDB.databaseConnection();

    public void back1Action(ActionEvent actionEvent) throws IOException {
        URL url = new File("src/ru/mip/database/fxmlfiles/database.fxml").toURI().toURL();
        AnchorPane pane = FXMLLoader.load(url);
        anchor_pane_add.getChildren().setAll(pane);
    }

    public void save1Action(ActionEvent actionEvent) {
        try {
            String id = field_id1.getText();
            String group = field_group1.getText();
            String faculty = field_faculty.getText();

            Tooltip tooltip = new Tooltip();
            tooltip.setText("Поле должно быть заполнено");

            String insertValues = "INSERT INTO CLASS(ID, NAME, FACULTY) VALUES ('";
            String insertFields = id + "','" + group + "','" + faculty + "')";
            String insertAdd = insertValues + insertFields;

            Statement statement = connection.createStatement();
            statement.executeUpdate(insertAdd);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void update1Action(ActionEvent actionEvent) throws SQLException {
        String id = field_id1_changed.getText();
        String group = field_group1_changed.getText();
        String faculty = field_faculty_changed.getText();

        if (!group.isEmpty()){
            String insertValues = "UPDATE CLASS SET " +
                    "NAME = '" + group +"'" +
                    " WHERE ID = '" + id + "'";

            Statement statement = connection.createStatement();
            statement.executeUpdate(insertValues);
        }
        if (!faculty.isEmpty()){
            String insertValues = "UPDATE CLASS SET " +
                    "FACULTY = '" + faculty + "'" +
                    " WHERE ID = '" + id + "'";

            Statement statement = connection.createStatement();
            statement.executeUpdate(insertValues);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }
}
