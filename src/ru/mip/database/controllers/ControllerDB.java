package ru.mip.database.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import ru.mip.database.connections.ConnectionDB;
import ru.mip.database.tables.TableGroupModel;
import ru.mip.database.tables.TableStudentsModel;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.util.ResourceBundle;

public class ControllerDB implements Initializable {

    public TableView<TableStudentsModel> table_people;
    public TableColumn<TableStudentsModel, Integer> stident_id;
    public TableColumn<TableStudentsModel, String> student_name;
    public TableColumn<TableStudentsModel, String> student_group;
    public TableColumn<TableStudentsModel, Integer> student_score;
    public TableColumn<TableStudentsModel, String> student_birth;
    public TableColumn<TableStudentsModel, Integer> student_sex;

    public Button button_close;
    public Button button_exit;
    public Button button_add;
    public Button button_delete;
    public AnchorPane anchor_pane2;

    public TableColumn<TableGroupModel, Integer> customer_id;
    public TableColumn<TableGroupModel, String> customer_name;
    public TableColumn<TableGroupModel, String> customer_faculty;
    public TableView<TableGroupModel> table_group;
    public Tab tab_students;
    public Tab tab_groups;
    public TabPane tab_pane;
    public Button button_change;
    public Button button_add1;
    public AnchorPane anchor_pane_action;
    public Button button_delete1;
    public Button button_change1;
    public TextField field_search;
    public TextField field_search1;
    public Label label_error;
    public Label label_error1;
    ConnectionDB connectionDB = new ConnectionDB();
    Connection connection = connectionDB.databaseConnection();

    ObservableList<TableStudentsModel> listView = FXCollections.observableArrayList();
    ObservableList<TableGroupModel> list = FXCollections.observableArrayList();

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        button_change1.setOnMouseEntered(e -> button_change1.setCursor(Cursor.OPEN_HAND));
        button_exit.setOnMouseEntered(e -> button_exit.setCursor(Cursor.OPEN_HAND));
        button_change.setOnMouseEntered(e -> button_change.setCursor(Cursor.OPEN_HAND));
        button_delete.setOnMouseEntered(e -> button_delete.setCursor(Cursor.OPEN_HAND));
        button_delete1.setOnMouseEntered(e -> button_delete1.setCursor(Cursor.OPEN_HAND));
        button_add.setOnMouseEntered(e -> button_add.setCursor(Cursor.OPEN_HAND));
        button_add1.setOnMouseEntered(e -> button_add1.setCursor(Cursor.OPEN_HAND));
        button_close.setOnMouseEntered(e -> button_close.setCursor(Cursor.OPEN_HAND));

        Tooltip tooltipDelete = new Tooltip();
        tooltipDelete.setText("Нажмите на студента в таблице,\n " +
                "а затем на кнопку \"Удалить\"");
        button_delete.setTooltip(tooltipDelete);

        Tooltip tooltipDelete1 = new Tooltip();
        tooltipDelete1.setText("Нажмите на группу в таблице,\n " +
                "а затем на кнопку \"Удалить\"");
        button_delete1.setTooltip(tooltipDelete1);

        Tooltip tooltipUpdate = new Tooltip();
        tooltipUpdate.setText("Обновление по ID");
        button_change.setTooltip(tooltipUpdate);
        button_change1.setTooltip(tooltipUpdate);

        Tooltip tooltipRefresh = new Tooltip();
        tooltipRefresh.setText("Обновить БД");

        filtredDataStudents();

        filtredDataGroups();
    }

    private void filtredDataGroups() {
        customer_id.setCellValueFactory(new PropertyValueFactory<>("id"));
        customer_name.setCellValueFactory(new PropertyValueFactory<>("name"));
        customer_faculty.setCellValueFactory(new PropertyValueFactory<>("faculty"));

        try {
            ConnectionDB connectionDB = new ConnectionDB();
            Connection connection = connectionDB.databaseConnection();

            String sqlCustomers = "SELECT * FROM CLASS";
            Statement s1 = connection.createStatement();
            ResultSet r1 = s1.executeQuery(sqlCustomers);

            while (r1.next()){
                list.add(new TableGroupModel(
                        r1.getString("id"),
                        r1.getString("name"),
                        r1.getString("faculty")
                ));
            }
            table_group.setItems(list);
            tab_groups.setContent(table_group);

            FilteredList<TableGroupModel> filteredList = new FilteredList<>(list, e -> true);
            field_search1.textProperty().addListener(((observable, oldValue, newValue) -> {
                filteredList.setPredicate(employee -> {
                    if (newValue == null || newValue.isEmpty()){
                        return true;
                    }
                    String lowerCaseFilter = newValue.toLowerCase();
                    if (employee.getName().toLowerCase().contains(lowerCaseFilter)){
                        return true;
                    } else if (employee.getFaculty().toLowerCase().contains(lowerCaseFilter)){
                        return true;
                    } else if (employee.getId().toLowerCase().contains(lowerCaseFilter)){
                        return true;
                    }
                    else
                        return false;
                });
            }));
            SortedList<TableGroupModel> sortedList = new SortedList<>(filteredList);
            sortedList.comparatorProperty().bind(table_group.comparatorProperty());
            table_group.setItems(sortedList);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void filtredDataStudents(){
        stident_id.setCellValueFactory(new PropertyValueFactory<>("id"));
        student_name.setCellValueFactory(new PropertyValueFactory<>("name"));
        student_group.setCellValueFactory(new PropertyValueFactory<>("group"));
        student_score.setCellValueFactory(new PropertyValueFactory<>("score"));
        student_birth.setCellValueFactory(new PropertyValueFactory<>("birthday"));
        student_sex.setCellValueFactory(new PropertyValueFactory<>("sex"));

        try {
            String sql = "SELECT * FROM PEOPLE";
            Statement s = connection.createStatement();
            ResultSet r = s.executeQuery(sql);

            while (r.next()){
                listView.add(new TableStudentsModel(
                        r.getString("ID"),
                        r.getString("NAME"),
                        r.getString("CLASS"),
                        r.getString("SCORE"),
                        r.getString("BIRTHDAY"),
                        r.getString("SEX")));
            }
            table_people.setItems(listView);
            tab_students.setContent(table_people);

            FilteredList<TableStudentsModel> filteredList = new FilteredList<>(listView, e -> true);
            field_search.textProperty().addListener(((observable, oldValue, newValue) -> {
                filteredList.setPredicate(employee -> {
                    if (newValue == null || newValue.isEmpty()){
                        return true;
                    }
                    String lowerCaseFilter = newValue.toLowerCase();
                    if (employee.getName().toLowerCase().contains(lowerCaseFilter)){
                        return true;
                    } else if (employee.getGroup().toLowerCase().contains(lowerCaseFilter)){
                        return true;
                    } else if (employee.getId().toLowerCase().contains(lowerCaseFilter)){
                        return true;
                    } else if (employee.getBirthday().toLowerCase().contains(lowerCaseFilter)){
                        return true;
                    } else if (employee.getScore().toLowerCase().contains(lowerCaseFilter)){
                        return true;
                    } else return employee.getSex().toLowerCase().contains(lowerCaseFilter);
                });
            }));
            SortedList<TableStudentsModel> sortedList = new SortedList<>(filteredList);
            sortedList.comparatorProperty().bind(table_people.comparatorProperty());
            table_people.setItems(sortedList);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void buttonClose(javafx.event.ActionEvent actionEvent) throws IOException {
        URL url = new File("src/ru/mip/database/fxmlfiles/login.fxml").toURI().toURL();
        AnchorPane pane = FXMLLoader.load(url);
        anchor_pane2.getChildren().setAll(pane);
    }

    public void exitAction(javafx.event.ActionEvent actionEvent) {
        System.exit(0);
    }

    public void addAction(javafx.event.ActionEvent actionEvent) throws IOException {
        Node node;
        URL url = new File("src/ru/mip/database/fxmlfiles/anchor_action_students.fxml").toURI().toURL();
        node = FXMLLoader.load(url);
        anchor_pane_action.getChildren().setAll(node);
    }

    public void addAction1(ActionEvent actionEvent) throws IOException {
        Node node;
        URL url = new File("src/ru/mip/database/fxmlfiles/anchor_action_group.fxml").toURI().toURL();
        node = FXMLLoader.load(url);
        anchor_pane_action.getChildren().setAll(node);
    }

    public void changeAction(ActionEvent actionEvent) throws IOException {
        Node node;
        URL url = new File("src/ru/mip/database/fxmlfiles/anchor_change_students.fxml").toURI().toURL();
        node = FXMLLoader.load(url);
        anchor_pane_action.getChildren().setAll(node);
    }

    public void deleteAction(ActionEvent actionEvent) throws SQLException, IOException {
        label_error.setVisible(table_people.getSelectionModel().isEmpty());
        String sql = "delete from people where id = " + table_people.getSelectionModel().getSelectedItem().getId();
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.execute();

        Node node;
        URL url = new File("src/ru/mip/database/fxmlfiles/database.fxml").toURI().toURL();
        node = FXMLLoader.load(url);
        anchor_pane_action.getChildren().setAll(node);
    }

    public void delete1Action(ActionEvent actionEvent) throws SQLException, IOException {
        label_error1.setVisible(table_group.getSelectionModel().isEmpty());
        String sql = "delete from class where id = " + table_group.getSelectionModel().getSelectedItem().getId();
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.execute();

        Node node;
        URL url = new File("src/ru/mip/database/fxmlfiles/database.fxml").toURI().toURL();
        node = FXMLLoader.load(url);
        anchor_pane_action.getChildren().setAll(node);
    }

    public void change1Action(ActionEvent actionEvent) throws IOException {
        Node node;
        URL url = new File("src/ru/mip/database/fxmlfiles/anchor_change_group.fxml").toURI().toURL();
        node = FXMLLoader.load(url);
        anchor_pane_action.getChildren().setAll(node);
    }
}