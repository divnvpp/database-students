package ru.mip.database.controllers;

import animatefx.animation.BounceIn;
import animatefx.animation.FadeIn;
import animatefx.animation.FadeOut;
import animatefx.animation.ZoomIn;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import ru.mip.database.connections.ConnectionDB;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

public class SignInCustomerController implements Initializable {
    public PasswordField field_password;
    public TextField field_login;
    public Button button_enter;
    public Button button_registration;
    public Button button_back;
    public Button button_exit;
    public AnchorPane anchor_pane;
    public Label label_error;
    public ImageView image_view;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        button_back.setOnMouseEntered(e -> button_back.setCursor(Cursor.OPEN_HAND));
        button_enter.setOnMouseEntered(e -> button_enter.setCursor(Cursor.OPEN_HAND));
        button_exit.setOnMouseEntered(e -> button_exit.setCursor(Cursor.OPEN_HAND));
        button_registration.setOnMouseEntered(e -> button_registration.setCursor(Cursor.OPEN_HAND));
        new FadeIn(anchor_pane).play();
        new BounceIn(image_view).play();
    }

    public void enterAction(ActionEvent actionEvent) throws SQLException, IOException {
        new ZoomIn(button_enter).play();
        if(!field_login.getText().isBlank() && !field_password.getText().isBlank()){
            validateLogin();
        }else {
            label_error.setText("Поля не могут быть пустыми");
        }
    }

    private void validateLogin() throws SQLException, IOException {
        ConnectionDB connectionDB = new ConnectionDB();
        Connection connection = connectionDB.databaseConnection();

        String username = field_login.getText();
        String password = field_password.getText();

        String verify = "select count(*) from login where username = '" + username + "' and password = '" + password + "'";

        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(verify);

        while (resultSet.next()){
            if (resultSet.getInt(1) == 1){
                URL url = new File("src/ru/mip/database/fxmlfiles/database.fxml").toURI().toURL();
                AnchorPane pane = FXMLLoader.load(url);
                anchor_pane.getChildren().setAll(pane);
            } else {
                label_error.setText("Неверный логин и/или пароль!");
            }
        }
    }

    public void registrationAction(ActionEvent actionEvent) throws IOException {
        URL url = new File("src/ru/mip/database/fxmlfiles/registration.fxml").toURI().toURL();
        AnchorPane pane = FXMLLoader.load(url);
        anchor_pane.getChildren().setAll(pane);
    }

    public void backAction(ActionEvent actionEvent) throws IOException {
        new FadeIn(anchor_pane).play();
        URL url = new File("src/ru/mip/database/fxmlfiles/login.fxml").toURI().toURL();
        AnchorPane pane = FXMLLoader.load(url);
        anchor_pane.getChildren().setAll(pane);
    }

    public void exitAction(ActionEvent actionEvent) {
        System.exit(0);
    }
}
