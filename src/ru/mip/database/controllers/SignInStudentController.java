package ru.mip.database.controllers;

import animatefx.animation.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import ru.mip.database.connections.ConnectionDB;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

public class SignInStudentController implements Initializable {

    public AnchorPane anchor_pane;
    public Button button_exit;
    public TextField field_login_student;
    public PasswordField field_password_student;
    public Button button_enter_student;
    public Button button_registration_student;
    public Button button_back_student;
    public Label label_error;
    public ImageView image_view;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        button_enter_student.setOnMouseEntered(e -> button_enter_student.setCursor(Cursor.OPEN_HAND));
        button_back_student.setOnMouseEntered(e -> button_back_student.setCursor(Cursor.OPEN_HAND));
        button_exit.setOnMouseEntered(e -> button_exit.setCursor(Cursor.OPEN_HAND));
        button_registration_student.setOnMouseEntered(e -> button_registration_student.setCursor(Cursor.OPEN_HAND));
        new FadeIn(anchor_pane).play();
        new BounceIn(image_view).play();
    }

    public void exitAction(ActionEvent actionEvent) {
        System.exit(0);
    }

    public void enterAction(ActionEvent actionEvent) throws IOException, SQLException {
        new ZoomIn(button_enter_student).play();
        if(!field_login_student.getText().isBlank() && !field_password_student.getText().isBlank()){
            validateLogin();
        }else {
            label_error.setText("Поля не могут быть пустыми");
        }
    }

    private void validateLogin() throws SQLException, IOException {
        ConnectionDB connectionDB = new ConnectionDB();
        Connection connection = connectionDB.databaseConnection();

        String username = field_login_student.getText();
        String password = field_password_student.getText();

        String verify = "select count(*) from students where username = '" + username + "' and password = '" + password + "'";

        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(verify);

        while (resultSet.next()) {
            if (resultSet.getInt(1) == 1) {
                URL url = new File("src/ru/mip/database/fxmlfiles/database_for_students.fxml").toURI().toURL();
                AnchorPane pane = FXMLLoader.load(url);
                anchor_pane.getChildren().setAll(pane);
            } else {
                label_error.setText("Неверный логин и/или пароль!");
            }
        }
    }

    public void registrationAction(ActionEvent actionEvent) throws IOException {
        URL url = new File("src/ru/mip/database/fxmlfiles/registration_for_students.fxml").toURI().toURL();
        AnchorPane pane = FXMLLoader.load(url);
        anchor_pane.getChildren().setAll(pane);
    }

    public void backAction(ActionEvent actionEvent) throws IOException {
        new FadeIn(anchor_pane).play();
        URL url = new File("src/ru/mip/database/fxmlfiles/login.fxml").toURI().toURL();
        AnchorPane pane = FXMLLoader.load(url);
        anchor_pane.getChildren().setAll(pane);
    }
}
