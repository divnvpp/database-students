package ru.mip.database.connections;

import java.sql.Connection;
import java.sql.SQLException;

import static java.sql.DriverManager.getConnection;

public class ConnectionDB {

    private static final String URL_DB = "jdbc:h2:tcp://localhost/~/students";
    private static final String USERNAME = "admin";
    private static final String PASSWORD = "admin";

    public Connection databaseConnection(){
        try {
            Class.forName("org.h2.Driver");
            return getConnection(URL_DB, USERNAME, PASSWORD);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}